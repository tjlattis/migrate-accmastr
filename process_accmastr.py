"""This is a omnibus script which takes in data from and exported, cleaned up version of accmastr"""
"""then create new json base on that data and post to the ArchivesSpace API"""

import json
import math
import openpyxl
import random
import re
import sys
import time
from asnake.client import ASnakeClient

__author__ = "Tony Lattis"
__email__ = "alattis@wisc.edu"

def create_date(acc_no_obj, date_obj, i):

    # if the field is a valid year, that becomes accession date
    match_obj = re.search("^\d\d\d\d", date_obj.value)
    if match_obj:
        accession_date = str(date_obj.value) + "-01-01"
        return accession_date

    # if the field is a valid six digit date, that becaomes the accession date
    match_obj = re.search("(\d\d)\/(\d\d)\/(\d\d)", date_obj.value)
    if match_obj:
        res = re.findall("(\d\d)\/(\d\d)\/(\d\d)", date_obj.value)
        accession_date = "20" + res[0][2] + "-" + res[0][0] + "-" + res[0][1]
        return accession_date

    # for use below, extract year from accession no
    year_from_acc = ""
    res = re.findall("(\d\d\d\d)\/(\d\d\d)\w?", acc_no_obj.value)
    if res:
        year_from_acc = res[0][0]

    # if the filed is a valid month/day pair, that plus the year from the accession no becomes the accession date
    match_obj = re.search("^\d\d\/\d\d$", date_obj.value)
    if match_obj:
        if date_obj.value == "00/00":
            accession_date = year_from_acc + "-01-01"
            return accession_date
        else:
            res = re.findall("(\d\d)\/(\d\d)", date_obj.value)
            month = res[0][0]
            if str(res[0][1]) == '00':
                day = '01'
            else:
                day = res[0][1]
            accession_date = year_from_acc + "-" + month + "-" + day
            return accession_date
        
    # if the field is a valid month, the first of that month plus the year from the accession no becomes the accession date
    match_obj = re.search("(\d\d)\/?", date_obj.value)
    if match_obj:
        res = re.findall("(\d\d)\/?", date_obj.value)
        accession_date = year_from_acc + "-" + res[0] + "-01"
        return accession_date

    # if the field is junk, january 1 of the year from the accession no becomes the date
    accession_date = str(year_from_acc) + "-01-01"
    return accession_date

def test_inventory(inv_obj):

    # true false test for inventory 
    if inv_obj.value is False:
        inventory = 'No'
    elif inv_obj.value == True:
        inventory = 'Yes'
    return inventory

def create_provenance(contact_obj, date):
    
    if contact_obj.value is None:
        return None
    # if there is a valid contact assemble a provenance statement
    else: 
        res = re.search("(\w+), (\w+.*)", contact_obj.value)
        if res is None:
            provenance = "Source: " + contact_obj.value + ", " + date  
        else:
            provenance = "Source: " + res.group(2) + " " + res.group(1) + ", " + date
        return provenance 

def create_extent(size_obj, i):

    # if there is no value just return None
    if size_obj.value is None:
        return None

    # catch '0' extents:
    elif size_obj.value == '0000' or size_obj.value == '00xF':
        return None

    # catch no-value digital
    elif size_obj.value == 'Digital' or size_obj.value == 'Birge':
        return [None, 'Digital']

    # try to find and assign values for normal accmastr situations
    else:

        res = re.search("(\d?\d?\.?\d\.?\d) ?(\w\w?)\*?$", size_obj.value)

        # here we deal with record storage cartons (1 foot each)
        if res.group(2).upper() == 'R' or res.group(2).upper() == 'B':
            lfeet = float(res.group(1))
            statement = ""
            if lfeet.is_integer():
                lfeet = int(lfeet)
            if lfeet == 1:
                statement = str(lfeet) + " record storage carton."
            else: 
                statement = str(lfeet) + " record storage cartons."
            return [lfeet, statement]

        # here we deal with letter document boxes
        elif res.group(2).upper() == 'A':
            number = float(res.group(1))
            lfeet = number * 0.5
            statement = ""
            if number.is_integer():
                number = int(number)
            if lfeet.is_integer():
                lfeet = int(lfeet)
            if number == 1:
                statement = str(number) + " letter document box." 
            else: 
                statement = str(number) + " letter document boxes."
            return [lfeet, statement]

        # here we deal with envelopes
        elif res.group(2).upper() == 'E':
            number = float(res.group(1))
            lfeet = number * 0.5
            statement = ""
            if number.is_integer():
                number = int(number)
            if lfeet.is_integer():
                lfeet = int(lfeet)
            if number == 1:
                statement = str(number) + " envelope." 
            else: 
                statement = str(number) + " envelopes."
            return [lfeet, statement]

        # here we deal with films
        elif res.group(2).upper() == 'F':
            number = float(res.group(1))
            lfeet = number * 0.165
            lfeet = round(lfeet, 2)
            statement = ""
            if number.is_integer():
                number = int(number)
            if lfeet.is_integer():
                lfeet = int(lfeet)
            if number == 1:
                statement = str(number) + " film." 
            else: 
                statement = str(number) + " films."
            return [lfeet, statement]

        # here we deal with volumes and items
        # we will just give each 0.1 linear feet
        elif res.group(2).upper() == 'I':
            number = float(res.group(1))
            lfeet = number * 0.1
            lfeet = round(lfeet, 1)
            if number.is_integer():
                number = int(number)
            if lfeet.is_integer():
                lfeet = int(lfeet)
            if number == 1:
                statement = str(number) + " item."
            else:
                statement = str(number) + " items."
            return [lfeet, statement]
        elif res.group(2).upper() == 'V': 
            number = float(res.group(1))
            lfeet = number * 0.1
            lfeet = round(lfeet, 1)
            if number.is_integer():
                number = int(number)
            if lfeet.is_integer():
                lfeet = int(lfeet)
            if number == 1:
                statement = str(number) + " volume."
            else:
                statement = str(number) + " volumes."
            return [lfeet, statement]
        elif res.group(2).upper() == 'G':
            return [None, 'Digital']

        # here we deal with photos, tapes, slides, and microfilms, DVDs, and CDs, and LPs
        elif res.group(2).upper() == 'P':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' photo.']
                else:
                    return [None, str(number) + ' photos.']
        elif res.group(2).upper() == 'T':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' tape.']
                else:
                    return [None, str(number) + ' tapes.']
        elif res.group(2).upper() == 'S':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' slide.']
                else:
                    return [None, str(number) + ' slides.']
        elif res.group(2).upper() == 'M':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' microfilm.']
                else:
                    return [None, str(number) + ' microfilms.']
        elif res.group(2).upper() == 'C':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' compact disk.']
                else:
                    return [None, str(number) + ' compact disks.']
        elif res.group(2).upper() == 'D':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' digital video disk.']
                else:
                    return [None, str(number) + ' digital video disks.']
        elif res.group(2).upper() == 'L':
            number = res.group(1)
            if float(number).is_integer():
                number = int(number)
                if int(number) == 1:
                    return [None, str(number) + ' long playing records.']
                else:
                    return [None, str(number) + ' long playing records.']

        # this is the bottom of the tumble, we should never make it here
        else:
            print("Problem with line " + i +". Extent tumbledown reached bottom")

def create_mat_date(span_obj, i):

    if span_obj.value == None:
        return None
    else:
        # in most cases date will be a span
        match_obj = re.search("^(\d\d\d\d)[-,\/](\d\d\d\d)$", span_obj.value)
        if match_obj:
            begin = match_obj.group(1)
            end = match_obj.group(2)
            if int(begin) > int(end):
                print(str(i) + ": INVALID SPAN " + str(span_obj.value))
            return ["inclusive", begin, end, "y"]

        # here we catch single date values
        match_obj = re.search("^-?(\d\d\d\d)-?$", span_obj.value)
        if match_obj:
            return ["single", match_obj.group(1), None, "y"]

        # here we catch approximate spans
        match_obj = re.search("ca ?(\d\d\d\d)-(\d\d\d\d)", span_obj.value)
        if match_obj:
            begin = match_obj.group(1)
            end = match_obj.group(2)
            if begin >= end:
                print(str(i) + "Invalid span " + str(span_obj.value))
            else:
                return ["inclusive", begin, end, 'approximate']

        # here we catch single aproximate values
        match_obj = re.search("ca?.? ?(\d\d\d\d)$", span_obj.value)
        if match_obj:
            return ["single", match_obj.group(1), None, 'approximate']
        match_obj = re.search("(\d\d\d\d)\?", span_obj.value)
        if match_obj:
            return ["single", match_obj.group(1), None, 'approximate']

        # here we catch decade values
        match_obj = re.search("(\d)(\d)(\d)\d'?s$", span_obj.value)
        if match_obj:
            decade = str(match_obj.group(1)) + str(match_obj.group(2)) + str(match_obj.group(3))
            begin = decade + '0'
            end = decade + '9'
            return ["inclusive", begin, end, 'approximate']

        # here we catch no date
        match_obj = re.search("^n\.?d\.?", span_obj.value)
        if match_obj:
            # do something with no date given
            return None

def print_record(title, content, disposition, inventory, acc_date, provenance, location, id0, extent, mat_date):

    print(id0)
    print("title: " + title)
    if content:
        print("content: " + content)
    if mat_date:
        if mat_date[3] == 'y':
            if mat_date[0] == 'single':
                print("date: " + str(mat_date[1]))
            elif mat_date[0] == 'inclusive':
                print("date: " + str(mat_date[1]) + " - " + str(mat_date[2]))
        elif mat_date[3] == 'approximate':
            if mat_date[0] == 'single':
                print("date: circa " + str(mat_date[1]))
            elif mat_date[0] == 'inclusive':
                print("date: circa " + str(mat_date[1]) + " - " + str(mat_date[2]))
    else:
        print("date: no date given" )
    if disposition:
        print("disposition: " + disposition)
    if inventory == 'Yes':
        print("inventory exists")
    elif inventory == 'No':
        print("no inventory")
    print("date of accession: " + acc_date)
    if provenance:
        print(provenance)
    if location:
        print("location: " + location)
    if extent:
        print(str(extent[0]) + " linear feet. " + extent[1])
    print()

def print_data(title, content, disposition, inventory, acc_date, provenance, location, id0, extent, mat_date, string):

    print("title: " + title)
    print("content: " + str(content))
    print("disposition: " + str(disposition))
    print("inventory: " + str(inventory))
    print("acc_date: " + acc_date)
    print("provenance: " + str(provenance))
    print("location: " + str(location))
    print("id0: " + id0)
    print("extent: " + str(extent))
    print("mat_date: " + str(mat_date))
    print(string)

def to_string(acc_no, date, group, office, description, span, size, contact, address, phone, location, P, comment, UDDS, R, code, inv):

    # this function concatinates all fields in the source data to add to the resulting record as a fall back for bad comprehension 
    string = "ACCNO|" + acc_no
    string = string + " DATE|" + str(date)
    string = string + " GRP|" + str(group)
    string = string + " OFFICE|" + str(office)
    string = string + " DESCRIPTION|" + description
    string = string + " SPAN|" + str(span)
    string = string + " SIZE|" + str(size)
    string = string + " CONTACT|" + str(contact)
    string = string + " ADDRESS|" + str(address)
    string = string + " PHONE|" + str(phone)
    string = string + " LOCATION|" + str(location)
    string = string + " P|" + str(P)
    string = string + " COMMENT|" + str(comment)
    string = string + " UDDS|" + str(UDDS)
    string = string + " R|" + str(R) 
    string = string + " code|" + str(code)
    string = string + " inv|" + str(inv)
    return string

def encode_as_json(title, content, disposition, inventory, acc_date, provenance, location, id0, extent, mat_date, string):

    # load the form into memory
    with open('acc_form.json', 'r') as source: 
        accession = json.load(source)

        # make replacement edits
        accession["title"] = title
        accession["display_string"] = title
        accession["content_description"] = content
        accession["disposition"] = disposition
        accession["inventory"] = inventory
        accession["provenance"] = provenance
        accession["general_note"] = "Located in: " + str(location) + "\r\n" + str(string) 
        accession["accession_date"] = acc_date
        accession["id_0"] = id0
        
        # check for an extent and modfy if needed
        # if not needed remove extent
        if extent:
            accession["extents"][0]["number"] = str(extent[0])(OCoLC)
            accession["extents"][0]["container_summary"] = extent[1]
        else:
            accession["extents"] = []

        # check if a date of materials exists and modify if needed
        # if no date of materials, delete dates data
        if mat_date:
            if mat_date[0] == 'single':
                accession["dates"][0]["expression"] = mat_date[1]
                accession["dates"][0]["begin"] = mat_date[1]
                accession["dates"][0]["end"] = mat_date[2]
                accession["dates"][0]["date_type"] = mat_date[0]
                accession["dates"][0]["certainty"] = mat_date[3]
            elif mat_date[0] == 'inclusive': 
                accession["dates"][0]["expression"] = mat_date[1] + " - " + mat_date[2]
                accession["dates"][0]["begin"] = mat_date[1]
                accession["dates"][0]["end"] = mat_date[2]
                accession["dates"][0]["date_type"] = mat_date[0]
                accession["dates"][0]["certainty"] = mat_date[3]
        else:
            accession["dates"] = []

        return accession

def post_record(newAccession):

    client = ASnakeClient()
    response = client.post('repositories/2/accessions', json=newAccession)
    return response

def main():
   
    # mount the spreadsheet
    input_file = "accmastr.xlsx"
    wb_obj = openpyxl.load_workbook(input_file)
    accmastr = wb_obj.active
    max_col = accmastr.max_column
    max_row = accmastr.max_row
    
    # lets go line by line
    for i in range(8491, max_row + 1):

        row = accmastr[i]

        # fields
        acc_no_obj = accmastr.cell(row = i, column = 1)
        date_obj = accmastr.cell(row = i, column = 2)
        group_obj = accmastr.cell(row = i, column = 3)
        office_obj = accmastr.cell(row = i, column = 4)
        description_obj = accmastr.cell(row = i, column = 5)
        span_obj = accmastr.cell(row = i, column = 6)
        size_obj = accmastr.cell(row = i, column = 7)
        contact_obj = accmastr.cell(row = i, column = 8)
        address_obj = accmastr.cell(row = i, column = 9)
        phone_obj = accmastr.cell(row = i, column = 10)
        location_obj = accmastr.cell(row = i, column = 11)
        p_obj = accmastr.cell(row = i, column = 12)
        comment_obj = accmastr.cell(row = i, column = 13)
        UDDS_obj = accmastr.cell(row = i, column = 14)
        R_obj = accmastr.cell(row = i, column = 15)
        code_obj = accmastr.cell(row = i, column = 16)
        inv_obj = accmastr.cell(row = i, column = 17)

        # assemble the fields based on that line
        title = description_obj.value + " [accmastr]"
        content = office_obj.value
        disposition = comment_obj.value
        inventory = test_inventory(inv_obj)
        acc_date = create_date(acc_no_obj, date_obj, i)
        provenance = create_provenance(contact_obj, acc_date)
        location = location_obj.value
        id0 = acc_no_obj.value
        # extent returns a list [lfeet, statement]
        extent = create_extent(size_obj, i)
        if extent == None:
            pass
        elif extent[0] == None:
            content = str(content) + " " + extent[1]
        elif extent[1] == 'Digital':
            content = content + " This collection is composed of digital materials"
        mat_date = create_mat_date(span_obj, i)
        # to string method
        string = to_string(acc_no_obj.value, date_obj.value, group_obj.value, office_obj.value, description_obj.value, span_obj.value, size_obj.value, contact_obj.value, address_obj.value, phone_obj.value, location_obj.value, p_obj.value, comment_obj.value, UDDS_obj.value, R_obj.value, code_obj.value, inv_obj.value)
        print_data(title, content, disposition, inventory, acc_date, provenance, location, id0, extent, mat_date, string)

        # now to encode all this as json
        json = encode_as_json(title, content, disposition, inventory, acc_date, provenance, location, id0, extent, mat_date, string)

        # post the record using ASnake
        response = post_record(json)
        if response.status_code == 200:
            print("SUCCESS!!")
            print()
        else:
            print("REQUEST FAILED ON LINE " + str(i))
            print(response.reason)
            sys.exit(0)

if __name__ == "__main__":
    start = time.time()
    main()
    end = time.time()
    total = round(end - start, 3)
    print("Total execution time: " + str(total) + " seconds")
